class Net
{

    call( target, method, body, headers = {'content-type': 'application/json' } )
    {

        //@ Convert the http verb to uppercase for easy evaluation
        method = method.toUpperCase();

        switch (method) 
        {
            case "POST":
            case "PUT":
            case "DELETE":
            
                //@ Ensure that the required headers are set
                headers["content-type"]     = 'application/json';
                headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : SMS_TOKEN;
                headers["SMS_DEV_KEY"]      = ( headers["SMS_DEV_KEY"] ) ? headers["SMS_DEV_KEY"] : SMS_DEV_KEY;
                headers["accept"]           = "application/json";
              
                // //@ Capture the constituent keys of the body object
                // let kys = Object.keys(body)

                // //@ Filter to check for any objects that might be arrays
                // let arrs = kys.filter(ky => Array.isArray( body[ky] ));

                // //@ Remove any null - valued arrays
                // arrs = arrs.filter(k => (body[k] != undefined && body[k] != null && body[k] != ""));

                // //@ Loop through each 'surviving' refrence key in the array object 
                // arrs.forEach( (a) => 
                // {  

                //     //@ loop through for as many keys as are in the array
                //     body[a].forEach( (value_at_body_a,body_a_index) => 
                //     {
                        
                //         //@ Capture the Object keys at the relevant index
                //         let obj_kys = Object.keys(body[a][body_a_index]);

                //         //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                //         if( obj_kys[0] != 0 )
                //         {

                //             //@ Loop through each available object key
                //             obj_kys.forEach( obj_ky => 
                //             {

                //                 // console.log( `${obj_ky} => ${body[a][0][obj_ky]}` )
                //                 //@ Populate the body object with the given parameter in the desired format
                //                 body[`${a}[${body_a_index}].${obj_ky}`] = body[a][body_a_index][obj_ky];

                //             });    

                //         }

                //     });

                //     // console.log(obj_kys)
                //     delete body[a];
                
                // });
            

                // let reque = qs.stringify(body,{arrayFormat: 'index'})

                let reque = str(body)
               
                return  request( {
                    uri     : `${SMS_URL}${target}`,
                    method  : method,
                    body    : reque,
                    headers : headers
                })
                .then(json);

            break;

            case "GET":

                //@ Ensure that the required headers are set
                headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : SMS_TOKEN;
                headers["SMS_DEV_KEY"]      = ( headers["SMS_DEV_KEY"] ) ? headers["SMS_DEV_KEY"] : SMS_DEV_KEY;
                headers["accept"]           = "application/json";

                return  request( {
                    uri : `${SMS_URL}${target}`,
                    qs: (body),
                    headers: headers
                })
                .then(json);

            break;
        
            default:
                return Promise.reject({ message : "An invalid http verb was specified for use with the SMS sdk @ direct calls."  });
            break;
        }           
        
    }

    custom( target, method, body, headers = {'content-type': 'application/json' } )
    {

        //@ Convert the http verb to uppercase for easy evaluation
        method = method.toUpperCase();

        switch (method) 
        {
            case "POST":
            case "PUT":
            case "DELETE":
            
                //@ Ensure that headers are set
                headers["content-type"]     = 'application/json';
                headers["authorization"]    = ( headers["authorization"] ) ? headers["authorization"] : SMS_TOKEN;
                headers["SMS_DEV_KEY"]      = ( headers["SMS_DEV_KEY"] ) ? headers["SMS_DEV_KEY"] : SMS_DEV_KEY;

                // //@ Capture the constituent keys of the body object
                // let kys = Object.keys(body)

                // //@ Filter to check for any objects that might be arrays
                // let arrs = kys.filter(ky => Array.isArray( body[ky] ));

                // //@ Remove any null - valued arrays
                // arrs = arrs.filter(k => (body[k] != undefined && body[k] != null && body[k] != ""));

                // //@ Loop through each 'surviving' refrence key in the array object 
                // arrs.forEach( (a) => 
                // {  

                //     //@ loop through for as many keys as are in the array
                //     body[a].forEach( (value_at_body_a,body_a_index) => 
                //     {
                        
                //         //@ Capture the Object keys at the relevant index
                //         let obj_kys = Object.keys(body[a][body_a_index]);

                //         //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                //         if( obj_kys[0] != 0 )
                //         {

                //             //@ Loop through each available object key
                //             obj_kys.forEach( obj_ky => 
                //             {

                //                 // console.log( `${obj_ky} => ${body[a][0][obj_ky]}` )
                //                 //@ Populate the body object with the given parameter in the desired format
                //                 body[`${a}[${body_a_index}].${obj_ky}`] = body[a][body_a_index][obj_ky];

                //             });    

                //         }

                //     });

                //     // console.log(obj_kys)
                //     delete body[a];
                
                // });
            

                // let reque = qs.stringify(body,{arrayFormat: 'index'})
                let reque = str(body)



                return  request( {
                    uri : `${target}`,
                    method: method,
                    body: reque,
                    headers: headers
                })
                .then(json);

            break;

            case "GET":
            
            //@ Ensure that the required headers are set
            headers["authorization"]        = ( headers["authorization"] ) ? headers["authorization"] : SMS_TOKEN;
            headers["SMS_DEV_KEY"]          = ( headers["SMS_DEV_KEY"] ) ? headers["SMS_DEV_KEY"] : SMS_DEV_KEY;

                return  request( {
                    uri : `${target}`,
                    qs: (body),
                    headers: headers
                })
                .then(json);
            break;
        
            default:
                return Promise.reject({ message : "An invalid http verb was specified for use with the SMS sdk @ custom url."  });
            break;
        }           
        
    } 

}

Object.assign( global,  {
    SMS_NETWORK : Net,
    SMS_NET     : new Net()
});