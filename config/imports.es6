 //@ Attach the native path module to the global space as "path"
 Object.assign( global, { path: require(`path`)});

//@ Attach the native fs module to the global scope as "fs"
 Object.assign(global, require("fs"))

//@ Attach the request-promise http module to the global scope as "request"
 Object.assign(global, {request: require("request-promise")});
 
//@ Attach the querystring http module to the global scope as "qs"
Object.assign(global, {qs: require("query-string")});



 //@ Import the project specific custom definitions
  require(path.join(__dirname,`/custom.es6`));

 //@ Import the network methods configurator
   require(path.join(__dirname,`/Net/net.js`));

//@ Include the generic basic function initiator
   require( path.join(__dirname,`/basics.es6`) );
