//@ Custom defined Globally accessible methods

Object.assign( global, {
    json        : ( obj ) => {
        try
        {
            return ( typeof(obj) === 'object' ) ? obj : JSON.parse( obj )
        }
        catch(Exc)
        {
            return obj;
        }
        
    },
    str         : ( obj ) =>
    {
        try
        {
            return ( typeof(obj) === "object" ) ? JSON.stringify(obj) : obj
        }
        catch(Exc)
        {
            return obj;
        }
    },
    clone       : (obj) => JSON.parse( JSON.stringify( obj ) ),
    jpsmstitle  : `\n[33mJAMBOPAY SMS[39m\t-`,
});



  /*
    DEFINE THE APPLICATION SPECIFIC ACCESS SUPER-GLOBALS
  */
    //@ Attach the sms main endpoint url to the global scope as "sms_url"
    Object.assign(global, { SMS_URL: 'http://sms.jambopay.co.ke' });    //  ["https://sms.jambopay.co.ke" for production]
 
    //@ Define the sms_token parameter
    Object.assign(global, { SMS_TOKEN : "" });

    //@ Define the SMS Developer Key Parameter
    Object.assign(global, { SMS_DEV_KEY : "" });

    //@ Define a custome response formatter as "make_response"
    Object.assign(global, {make_response: ( response=200, message="Sample Response", command="done") => 
        {
            return {
                response
                ,data : { message,command}
            }
        }
    });

//@ Attach the basic phone number validators to the global scope
Object.assign( global, require(path.join(__dirname,`/Validation/validators.es6`)) );
