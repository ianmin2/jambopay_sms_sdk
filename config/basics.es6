class Generic 
{
    
   
    //# Expects {email,password}
    login ( username, password )
    {

        console.log(`${jpsmstitle} ATTEMPTING A LOGIN.\n`);

        return new Promise( function(resolve,reject)
        {

            SMS_NET.call( '/auth/verify/', 'GET', { email: username, password } )
            .then( login_data => 
            {

                //! Set SMS_TOKEN and SMS_DEV_KEY global parameters

                 //@ Define the sms_token parameter
                SMS_TOKEN       = login_data.data.message.token;

                //@ Define the SMS Developer Key Parameter
                SMS_DEV_KEY     = login_data.data.message.key;
                                     
                resolve( login_data );

            })
            .catch( fail_data => {

                reject( fail_data );

            })

        });

    };
    
    //# Expects { endpointURL as string }
    set_endpoint_url ( endpointUrl="https://sms.jambopay.co.ke"  )
    {
        return new Promise( function(resolve,reject){

            endpointUrl = endpointUrl.replace(/\s|\/$/ig,"")
            Object.assign(global, { SMS_URL: endpointUrl });
            console.log(`${jpsmstitle} ACCESS URL SET TO '${SMS_URL}'\n`);
            resolve(`The endpoint url has successfuly been set to ${ endpointUrl}`);

        })
    
    }; 

}

module.exports = Generic;
Object.assign( global, { SMS_GENERIC : Generic } );