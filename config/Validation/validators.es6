//* DEFINE CUSTOM VALIDATORS

//!EMAIL VALIDATION
const mailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/ig;

//!PASSWORD VALIDATION
const passRegex = /^[-@./\!\$\%\^|#&,+\w\s]{6,50}$/ig;

//! EMAIL VALIDATION
const telRegex      = /^([\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}(?:,|$))+$/im;

const isTelephone   = ( prospective_telephone = "" ) => prospective_telephone.toString().match(telRegex); //.test(prospective_telephone);

const toTelephone   = ( prospective_telephone, phone_prefix = "254" ) => 
{

    let phone = prospective_telephone.replace(/\s|\(|\)|\-/ig,'');
    let rgx = new RegExp(`^${phone_prefix}`,'i');

    return isTelephone(prospective_telephone) 
            ? (phone.length <= 10 )
                ? (phone.charAt(0) == 0)
                    ? `${phone_prefix}${phone.replace(/^0/i, '')}`
                    : `${phone_prefix}${phone}`
                : (phone.charAt(0)(`+`) == true)
                    ? `${phone.replace(/\+/ig, '')}`
                    : ( rgx.test(phone) == true )
                ? phone
                : `${phone_prefix}${phone}`
            : undefined ;  
}


module.exports = {
    mailRegex,
    passRegex,
    telRegex,
    isTelephone,
    toTelephone
}