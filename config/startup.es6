/*
    IMPORT THE APPLICATION BASICS
*/
    require(`./imports.es6`);
    require(path.join(__dirname,`/../SMS/sms.es6`));

    //@ Assign the generic methods to the module
    Object.assign( exports, { 
                                sms_login               : SMS_GENERIC.login,  
                                set_sms_endpoint_url    : SMS_GENERIC.set_endpoint_url
                            });
