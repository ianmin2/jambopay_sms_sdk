/*
    For connection to the Jambopay SMS gateway
*/

//@ Allow the setting of endpoints with self signed certificates
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
require(`./config/startup.es6`);