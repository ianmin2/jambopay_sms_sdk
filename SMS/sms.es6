class SMS extends SMS_GENERIC
{
    
    //expects {senderName,country_code}
    constructor( senderName, country_code = "254" )
    {
        super();
        this.senderName         = senderName;
        this.country_code       = country_code;        
    }

    //! expects ("recipient","message",{start:timestamp,end:timestamp})
    sendOne( recipient, message, schedule )
    {

        let sendParams = {
           body     :
           {
                to      : toTelephone(recipient,this.country_code),
                text    : message 
           },
           sender   : this.senderName
        };

        if(sendParams.body.to)
        {

            if(schedule && typeof schedule == "object")
            {
                sendParams.initiate         = (schedule.start)  ? schedule.start : undefined;
                sendParams.complete_by      = (schedule.end)    ? schedule.end   : undefined;
            }

            console.log(sendParams);

            return SMS_NET.call(
                `/sms/one`,
                "POST",
                sendParams
            );

        }
        else
        {

            return Promise.reject(make_response(412,`Please define a proper SMS recipient. \n${recipient} does not fit the bill.`));

        }

    }

    //! expects ([{to,text},...],{start:timestamp,end:timestamp})
    sendMany( recipientArray, schedule )
    {

        if(Array.isArray(recipientArray))
        {

            recipientArray = recipientArray
            .map( recInfo => 
            {

                return  (Array.isArray(recInfo)) 
                    ?
                        {
                            to      : toTelephone( recInfo[0], this.country_code ),
                            text    : recInfo[1]
                        }
                    : undefined;

            })
            .filter( recipientInfo =>   (typeof recipientInfo == 'object') ?  ( recipientInfo.text && recipientInfo.to )  : false )
           
            console.dir(recipientArray);

            if( recipientArray.length > 0 )
            {

                let sendParams = {
                    body    : recipientArray,
                    sender  : this.senderName
                };
    
                if(schedule && typeof schedule == "object")
                {
                    sendParams.initiate         = (schedule.start)  ? schedule.start : undefined;
                    sendParams.complete_by      = (schedule.end)    ? schedule.end   : undefined;
                }
    
                return SMS_NET.call(
                    `/sms/many`,
                    "POST",
                    sendParams
                );

            }
            else
            {
                return Promise.reject(make_response(412,`The provide contacts don't match the criteria [[to,text],[to,text]]`,recipientArray));
            }

        }
        else
        {
            return Promise.reject(make_response(412,`Please provide the multiple SMS recipients data in the format [[to,text],[to,text]]`,recipientArray));
        }

    }

    getBalance()
    {
        return SMS_NET.call(
            `/sms`,
            "GET",
            {
                command : "get",
                table   : "vw_organization_balances"
            }
        );
    }

    getProfile()
    {
        return SMS_NET.call(
            `/auth/me`,
            "GET"
        );
    }

    recoverPassword( userEmail )
    {

        //@ Validate the provided email address
        if( mailRegex.test(userEmail) )
        {
            return SMS_NET.call(
                `/auth/passwords/forgot`,
                "GET",
                {
                   email    : userEmail
                }
            );
        }
        else
        {
            return Promise.reject(make_response(412,`Please provide a valid recovery email adderess.\n'${userEmail}' does not meet the required criteria.`));
        }

        
    }

    addContacts()
    {
        return undefined;
    }

    addGroups()
    {
        return undefined;
    }

    addGroupTemplate()
    {
        return undefined;
    }

}

module.exports = SMS;
Object.assign(global,{SMS});